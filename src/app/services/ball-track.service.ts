import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { BoxStateEnum}  from '../enums';
import { Box } from '../models';

@Injectable()
export class BallTrackService {
  private started = false;

  private boxesSubject = new BehaviorSubject<Box[]>([]);
  boxes$ = this.boxesSubject.asObservable();
  private get boxes(): Box[] {
    return this.boxesSubject.value;
  }
  private set boxes(value: Box[]) {
    this.boxesSubject.next(value);
  }

  constructor() { }

  isActive() {
    return this.started;
  }

  startBoard(boxCount: number = 4) {
    this.started = false;
    this.boxes = [];
    for (let i = 0; i < boxCount; i++) {
      this.boxes.push(new Box(i));
    }
    this.boxes[0].currentState = BoxStateEnum.ACTIVE;
  }

  activeBox(currentBox: Box) {
    this.started = true;
    const previousBox = this.boxes.find(box => box.isActive());
    previousBox.setShadow();
    currentBox.setActive();
    this.boxes
      .filter(box => box.id !== previousBox.id && box.id !== currentBox.id)
      .forEach(box => box.setEmpty());
  }
}