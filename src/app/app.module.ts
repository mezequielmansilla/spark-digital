import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { BoardComponent} from './components/board/board.component';
import { BoxComponent } from './components/box/box.component';
import { CircleComponent } from './components/circle/circle.component';
import { BallTrackService } from './services/ball-track.service';


@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, BoardComponent, BoxComponent, CircleComponent ],
  bootstrap:    [ AppComponent ],
  providers: [BallTrackService]
})
export class AppModule { }
