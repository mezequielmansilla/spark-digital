import { BoxStateEnum } from '../enums';

export class Box {
  id: number;
  currentState: BoxStateEnum = BoxStateEnum.EMPTY;
  previousState: BoxStateEnum  = BoxStateEnum.EMPTY;
  constructor(id?: number) {
    this.id = id;
  }

  setActive() {
    this.currentState = BoxStateEnum.ACTIVE;
  }

  setShadow() {
    this.previousState = BoxStateEnum.ACTIVE;
    this.currentState = BoxStateEnum.EMPTY;
  }

  setEmpty() {
    this.previousState = BoxStateEnum.EMPTY;
    this.currentState = BoxStateEnum.EMPTY;
  }

  isActive() {
    return this.currentState === BoxStateEnum.ACTIVE;
  }
  isShadow() {
    return this.previousState === BoxStateEnum.ACTIVE;
  }
}