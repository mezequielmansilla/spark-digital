import { Component, OnInit, Input } from '@angular/core';

import { Observable } from 'rxjs';

import { BallTrackService } from '../../services/ball-track.service';
import { BoxStateEnum}  from '../../enums';
import { Box}  from '../../models';

@Component({
  selector: 'my-board',
  templateUrl: './board.component.html',
  styleUrls: [ './board.component.css' ]
})
export class BoardComponent implements OnInit {
  @Input()
  boxCount: number;
  get startText(): string {
    if (this.ballTrackService.isActive()) {
      return 'Restart';
    }
    return 'Start';
  }

  get boxes$(): Observable<Box[]> {
    return this.ballTrackService.boxes$;
  }

  constructor(private ballTrackService: BallTrackService) {
  }

  onStartClick() {
    this.ballTrackService.startBoard(this.boxCount);
  }

  ngOnInit() {
    this.ballTrackService.startBoard(this.boxCount);
  }

  onBoxClick(box: Box) {
    this.ballTrackService.activeBox(box);
  }
}
