import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { BoxStateEnum, CircleColorEnum }  from '../../enums';
import { Box }  from '../../models';

@Component({
  selector: 'my-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {
  @Input()
  box: Box;

  @Output()
  boxClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.boxClick.emit(this.box);
  }

  getCircleColor(): string {
    if (!this.box) {
      return '';
    }
    if (this.box.isActive()) {
      return CircleColorEnum.ACTIVE;
    }
    if (this.box.isShadow()) {
      return CircleColorEnum.SHADOW;
    }
    return '';
  }
}