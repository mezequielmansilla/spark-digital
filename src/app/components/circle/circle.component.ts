import { Component, OnInit, Input } from '@angular/core';

import { CircleColorEnum } from '../../enums';

@Component({
  selector: 'my-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css']
})
export class CircleComponent implements OnInit {
  @Input()
  color: CircleColorEnum;

  constructor() { }

  ngOnInit() {
  }

}